#ifndef CVL_PROFILING_H
#define CVL_PROFILING_H


#define CVL_TICN_CHOOSER(A,B,FUNC, ...)  FUNC
/**
 * Profiling macro capable of executing code block several times and calculating average execution
 * time.
 *
 * Marks start of profiled code block. Accepts one or two arguments.
 * Compared with CVL_TIC macro this macro implementation wraps profiled code with compound statement
 * (block). This makes macro potentially incompatible with CVL_TIC macro.
 *
 * Usage.
 * Single argument, execute code once:
 * CVL_TICN(tag);
 * ...code...
 * CVL_TOCN(tag)
 *
 * Two arguments, execute code count times and calculate average execution time.
 * CVL_TICN(tag, count);
 * ...code...
 * CVL_TOCN(tag)
 */
#define CVL_TICN(...) CVL_TICN_CHOOSER(__VA_ARGS__,\
                          CVL_TICN_BATCH(__VA_ARGS__),\
                          CVL_TICN_ONCE(__VA_ARGS__)\
                      )
/**
 * \def CVL_TOCN(tag)
 * Marks end of profiled code block.
 *
 * Companion macro for CVL_TICN(tag[, count])
 */

/**
 * \def CVL_TIC(tag)
 *
 * Measure code execution time.
 *
 * Marks start of profiled code block.
 *
 * Usage.
 * CVL_TIC(tag)
 * ...code...
 * CVL_TOC(tag)
 */

/**
 * \def CVL_TOC(tag)
 * Marks end of profiled code block.
 *
 * Companion macro for CVL_TIC(tag)
 */

#if CVL_PROFILING

#include <time.h>
#include <sys/time.h>
#ifdef __APPLE__
#include <mach/clock.h>
#include <mach/mach.h>
#endif
#include <stdio.h>

static inline void current_utc_time(struct timespec *ts) {
#ifdef __APPLE__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts->tv_sec = mts.tv_sec;
    ts->tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, ts);
#endif
}

#define CVL_TICN_ONCE(tag) \
struct timespec tag##_start_ts;\
const unsigned int tag##_batch_size = 1;\
current_utc_time(&tag##_start_ts);\
{

#define CVL_TICN_BATCH(tag, count) \
struct timespec tag##_start_ts;\
const unsigned int tag##_batch_size = count;\
current_utc_time(&tag##_start_ts);\
unsigned int tag##_batch_iteration;\
for (tag##_batch_iteration = 0; tag##_batch_iteration < tag##_batch_size; tag##_batch_iteration+=1)\
{

#define CVL_TOCN(tag) \
}\
struct timespec tag##_finish_ts;\
current_utc_time(&tag##_finish_ts);\
double tag##_elapsed = (tag##_finish_ts.tv_sec - tag##_start_ts.tv_sec);\
tag##_elapsed += (tag##_finish_ts.tv_nsec - tag##_start_ts.tv_nsec) / 1000000000.0;\
if (tag##_batch_size > 1) {\
    printf(#tag": %f sec. (over %d iterations)\n", tag##_elapsed / tag##_batch_size , tag##_batch_size);\
}\
else {\
    printf(#tag": %f sec.\n", tag##_elapsed);\
}


#define CVL_TIC(tag) \
struct timespec tag##_start_ts;\
current_utc_time(&tag##_start_ts);

#define CVL_TOC(tag) \
struct timespec tag##_finish_ts;\
current_utc_time(&tag##_finish_ts);\
double tag##_elapsed = (tag##_finish_ts.tv_sec - tag##_start_ts.tv_sec);\
tag##_elapsed += (tag##_finish_ts.tv_nsec - tag##_start_ts.tv_nsec) / 1000000000.0;\
printf(#tag": %f sec.\n", tag##_elapsed);\

#else

#define CVL_TIC(tag)
#define CVL_TOC(tag)
#define CVL_TICN_BATCH(tag, count)
#define CVL_TICN_ONCE(tag)
#define CVL_TOCN(tag)
#endif


#endif //CVL_PROFILING_H

